package model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Fetch;
import util.Role;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static org.hibernate.annotations.FetchMode.JOIN;

/**
 * @author Tarita Ana
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "User")
public class User {

    @Id
    @Column(name = "u_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    private Role role;
    @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(JOIN)
    private Set<Package> sentPackages = new HashSet<>();
    @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(JOIN)
    private Set<Package> receivedPackages = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Package> getSentPackages() {
        return sentPackages;
    }

    public void setSentPackages(Set<Package> packages) {
        this.sentPackages = packages;
    }

    public Set<Package> getReceivedPackages() {
        return receivedPackages;
    }

    public void setReceivedPackages(Set<Package> receivedPackages) {
        this.receivedPackages = receivedPackages;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
