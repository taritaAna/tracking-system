package model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.annotations.FetchMode.JOIN;

/**
 * @author Tarita Ana
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "Package")
public class Package {

    @Id
    @Column(name = "p_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "send_id")
    private User sender;
    @ManyToOne
    @JoinColumn(name = "dest_id")
    private User receiver;
    private String name;
    private String description;
    @ManyToOne
    @JoinColumn(name = "send_city_id")
    private City senderCity;
    @ManyToOne
    @JoinColumn(name = "dest_city_id")
    private City destCity;
    private boolean isTracked;
    @OneToMany(mappedBy = "aPackage", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(JOIN)
    private List<RouteEntry> route = new ArrayList<>();

    public City getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(City senderCity) {
        this.senderCity = senderCity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getDestCity() {
        return destCity;
    }

    public void setDestCity(City destCity) {
        this.destCity = destCity;
    }

    public boolean isTracked() {
        return isTracked;
    }

    public void setIsTracked(boolean isTracked) {
        this.isTracked = isTracked;
    }

    public List<RouteEntry> getRoute() {
        return route;
    }

    public void setRoute(List<RouteEntry> route) {
        this.route = route;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
