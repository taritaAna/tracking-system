package util;

/**
 * @author Tarita Ana
 */
public enum Role {
    ADMIN, CLIENT
}
